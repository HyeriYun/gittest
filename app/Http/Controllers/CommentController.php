<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Article;
use App\User;
use DateTime;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($u_id)
    {
        //
      $comments = Comment::where('user_id', $u_id)->orderBy('created_at', 'desc')->paginate(10);
      $user = User::find($u_id);
      $now = new Datetime;
      $nowstr = $now -> format('Ymd');
    //   $user = User::where('id', $u_id)->first();
      return view('Comment\index')->with('comments', $comments)->with('user', $user)->with('nowstr', $nowstr);

    }

    public function notification($u_id)
    {
        //
      $articles = Article::with('comments')->where('user_id', $u_id)->get();
      $user = User::find($u_id);
      $comments = array_collapse( array_pluck($articles, 'comments'));
      $now = new Datetime;
      $nowstr = $now -> format('Ymd');
    //   $user = User::where('id', $u_id)->first();
      return view('Comment\notification')->with('comments', $comments )->with('user', $user)->with('nowstr', $nowstr);

    }


//TEST
    // public function notification($u_id)
    // {
    //     //
    //   $user = User::find($u_id);
    //   $comments = Comment::orderBy('created_at', 'desc')->paginate(10);
    //   $now = new Datetime;
    //   $nowstr = $now -> format('Ymd');
    //   return view('Comment\notification')->with('comments', $comments )->with('user', $user)->with('nowstr', $nowstr);

    // }

//TEST

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($p_id, Request $request)
    {
        //
        $comment = new Comment;
        $comment->content=$request->input('content');
        $comment->article_id = $p_id;
        $comment->user_id = Auth::id();
        $comment->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($p_id, $id)
    {
        //
        $comment = Comment::find($id);
        if(Auth::id() == $comment->user_id)
            $comment->delete();

        return back();
    }
}
