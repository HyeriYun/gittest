<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Article;
use App\User;
use App\Comment;
use App\Category;
use App\Tag;
use DateTime;

class ArticleController extends Controller
{
//Article List Index
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $articles = Article::orderBy('created_at','desc')->orderBy('id','desc')->paginate(10);
        $comments = Comment::get();
        $categories = Category::get();
        $notices = Article::where('notice_chk','on')->orderBy('id','desc')->get();
        $now = new Datetime;
        $nowstr = $now -> format('Ymd');
        return view('Article\index')->with('articles', $articles)->with('nowstr', $nowstr)->with('comments', $comments)->with('categories', $categories)->with('notices',$notices);
        
    }
//ByUser
    public function indexByUser($u_id)
    {
        //
        $user = User::find($u_id);
        $articles = Article::where('user_id', $u_id)->orderBy('created_at','desc')->orderBy('id','desc')->paginate(10);
        $comments = Comment::get();
        $now = new Datetime;
        $nowstr = $now -> format('Ymd');
        return view('Article\indexBy')->with('articles', $articles)->with('nowstr', $nowstr)->with('comments', $comments)->with('user', $user)->with('u_id', $u_id);
        
    }
//ByCategory
    public function indexByCategory($c_id)
    {
        //
        $articles = Article::where('category_id', $c_id)->orderBy('created_at','desc')->orderBy('id','desc')->paginate(10);
        $comments = Comment::get();
        $categories = Category::get();
        $notices = Article::where('notice_chk','on')->orderBy('created_at','desc')->orderBy('id','desc')->get();
        $now = new Datetime;
        $nowstr = $now -> format('Ymd');
        return view('Article\index')->with('articles', $articles)->with('nowstr', $nowstr)->with('comments', $comments)->with('categories', $categories)->with('notices',$notices);
        
    }

//Tag
    public function tagIndex()
    {
        //
        $tags = Tag::orderby('name')->get();
        return view('Tag.index')->with('tags',$tags);
    }

    public function indexByTags($t_name)
    {
        //
        $tag = Tag::where('name', $t_name)->first();
        $articles = $tag->articles()->orderBy('created_at','desc')->orderBy('id','desc')->paginate(10);
        $comments = Comment::get();
        $categories = Category::get();
        $notices = Article::where('notice_chk','on')->orderBy('created_at','desc')->orderBy('id','desc')->get();
        $now = new Datetime;
        $nowstr = $now -> format('Ymd');
        return view('Article\indexBy')->with('articles', $articles)->with('nowstr', $nowstr)->with('comments', $comments)->with('categories', $categories)->with('notices',$notices)->with('tagname', $t_name)->with('tag', $tag);
        
    }
    
//BySearch
    public function search(Request $request)
    {
        //
        $field = $request->input('field');
        $search = $request->input('search');
        $articles = Article::where($field, 'like', '%' . $search . '%')->orderBy('created_at','desc')->orderBy('id','desc')->paginate(10);
        $comments = Comment::get();
        $categories = Category::get();
        $notices = Article::where('notice_chk','on')->orderBy('created_at','desc')->orderBy('id','desc')->get();
        $now = new Datetime;
        $nowstr = $now -> format('Ymd');
        return view('Article\search')->with('articles', $articles)->with('nowstr', $nowstr)->with('comments', $comments)->with('categories', $categories)->with('notices',$notices)->with('search', $search)->with('field', $field);
        
    }
//Article-create, Store
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::get();
        return view('Article\create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:30',
            'content' => '',
        ]);

        if ($validator->fails()) {
            return redirect('article/create')
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

        //Store in database
        $article = new Article;
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $category = Category::where('name', $request->input('category'))->first();
        $article->category_id = $category->id;
        $article->user_id = Auth::id();
        $article->notice_chk = $request->input('notice');
        $article->secret_chk = $request->input('secret');
        $tag_name = explode( '#' , $request->input('tags') );
        
        //태그 입력 값이 없을때
        if( $request->input('tags') == null ){
            $article->save();   
        }
        else{
            for( $i=1; $i<count($tag_name); $i++ ){
                if( Tag::where('name', $tag_name[$i])->count() == 0 ){
                    //입력받은 태그가 태그테이블에 존재하는지 확인하고 인서트
                    $tag = new Tag;
                    $tag->name = $tag_name[$i];
                    $tag->save();
                    //피벗 테이블에 아티클, 태그 아이디 저장
                    $tag->articles()->save( $article , ['tag_id' => $tag->id]);
                }else{
                    //이미 존재하는 태그면 그 태그에 아이디 가져오기
                    $tag_id = Tag::where('name', $tag_name[$i])->first()->id;                
                    //피벗 테이블에 아티클, 태그 아이디 저장
                    Tag::find($tag_id)->articles()->save( $article , ['tag_id' => $tag_id]);

                }
                
            }
        }
        return redirect('article');

        }
    }

//Article Show(상세페이지)
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article = Article::find($id);
        $tags = $article->tags()->get();
        if( Auth::check() ){
            $fav = $article->users()->where('user_id', Auth::user()->id)->count();
        }else{
            $fav = 0;
        }
        if( $article->secret_chk =='on' ){
            if( $article->user_id == Auth::id() ){
                $category = Category::where('id', $article->category_id)->first();
                return view('Article.show')->with('article', $article)->with('category', $category)->with('tags', $tags)->with('fav', $fav);
            }else{
                return redirect('article');
            }
        }else{
           $category = Category::where('id', $article->category_id)->first();
           return view('Article.show')->with('article', $article)->with('category', $category)->with('tags', $tags)->with('fav', $fav);
        };
        
    }
//Article edit Update
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $article = Article::find($id);
        $tags = $article->tags()->get();
        $selected = Category::where('id', $article->category_id )->first();
        $categories = Category::get();
        return view('Article.edit')->with('article', $article)->with('categories', $categories)->with('selected', $selected)->with('tags', $tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:30',
            'content' => '',
        ]);

        if ($validator->fails()) {
            return redirect('article'.'/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{
            
        //Update
        $article = Article::find($id);
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $category = Category::where('name', $request->input('category'))->first();
        $article->category_id = $category->id;
        $article->user_id = Auth::id();
        $article->notice_chk = $request->input('notice');
        $article->secret_chk = $request->input('secret');
        $tag_name = explode( '#' , $request->input('tags') );

        //태그제거
            //Article::find($id)->tags()->detach();
        //태그추가
        //태그 입력 값이 없을때
        if( $request->input('tags') == null ){ 
            $article->save();   
        }
        //태그 입력 값이 있을때
        else{
            for( $i=1; $i<count($tag_name); $i++ ){
                //원래 태그와 입력값을 비교해서 원래 태그가 입력값에 없으면 원래 태그를 삭제한다.
                $org_tags = Article::find($id)->tags()->get();
                    foreach( $org_tags as $org_tag ){
                        if( in_array( $org_tag->name, $tag_name ) != FALSE ){
                            //그대로라면 내버려둔다
                        }else{
                            //태그가 사라졌다면 원래 태그를 아티클 피벗에서 삭제한다.
                            Article::find($id)->tags()->detach( Tag::where('name', $org_tag->name)->first() );
                            //아티클이 없는 태그는 DB에서도 지운다.
                            if( Tag::where('name', $org_tag->name)->first()->articles()->count() == 0 ){
                            Tag::where('name', $org_tag->name)->delete();
                            };
                        };
                    }
                //입력받은 태그가 아티클 태그에 있다면
                if( Article::find($id)->tags()->where('name', $tag_name[$i])->count() != 0 ){ 
                    //다시 추가 안한다.  
                //입력받은 태그가 아티클 태그에 없으면
                }else{
                    //입력받은 태그가 DB에도 없다면 새로 추가.
                    if( Tag::where('name', $tag_name[$i])->count() == 0 ){
                        //입력받은 태그가 태그테이블에 존재하는지 확인하고 인서트
                        $tag = new Tag;
                        $tag->name = $tag_name[$i];
                        $tag->save();
                        //피벗 테이블에 아티클, 태그 아이디 업데이트
                        $tag->articles()->save( $article , ['tag_id' => $tag->id]);
                    //입력받은 태그가 DB에는 있다면 추가할 필요X
                    }else{
                        //이미 존재하는 태그면 그 태그에 아이디 가져오기
                        $tag_id = Tag::where('name', $tag_name[$i])->first()->id;                
                        //피벗 테이블에 아티클, 태그 아이디 업데이트
                        Tag::find($tag_id)->articles()->save( $article , ['tag_id' => $tag_id]);
                    };
                }
            }
        }
        
        return redirect('article/'.$id);
        }
    }
//Article Delete
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        $comments = Comment::where('article_id', $id);
        if(Auth::id() == $article->user_id)

            $comments->delete();
            $tags = $article->tags()->get();
            //Article에 달린 외래키 연결 끊기
            $article->tags()->detach();
            $article->delete();
            //글 지우면서 이 태그가 아티클이 없게된다면 DB에서 삭제
            foreach( $tags as $tag ){
                if( $tag->articles()->count() == 0 ){
                    $tag->delete();
                }
            }
              
        return redirect('article');
    }
}
