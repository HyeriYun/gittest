<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class article extends Model
{
    //
       public function comments()
    {
        return $this->hasMany('App\Comment');
    }

        public function user()
    {
        return $this->belongsTo('App\User');
    }
        
        public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

        public function category()
    {
        return $this->belongsTo('App\Category');
    }

        public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }
}
