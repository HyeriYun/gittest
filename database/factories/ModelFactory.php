<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(100),
    ];
});


$factory->define(App\Article::class, function (Faker\Generator $faker) {
    $user_id_min = App\User::all()->min()->id;
    $user_id_max = App\User::all()->max()->id;

    return [
        'title' => $faker->realText(20),
        'content' => $faker->text,
        'user_id' => $faker->numberBetween($user_id_min, $user_id_max),
        'category_id' => $faker->numberBetween(0,4),
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->unique()->word,
    ];
});


$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    $user_id_min = App\User::all()->min()->id;
    $user_id_max = App\User::all()->max()->id;
    $article_id_min = App\Article::all()->min()->id;
    $article_id_max = App\Article::all()->max()->id;

    return [
        'content' => $faker->text,
        'user_id' => $faker->numberBetween($user_id_min, $user_id_max),
        'article_id' => $faker->numberBetween($article_id_min, $article_id_max),
    ];
});

