<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 0)->create();

        factory(App\Article::class, 0)->create()->each(function($article) {
            factory(App\Tag::class, rand(1,5))->make()->each(function($tag) use($article) {
                $article->tags()->save($tag);
            });
        });

        factory(App\Tag::class, 0)->create()->each(function($tag) {
            factory(App\Article::class, rand(1,2))->make()->each(function($article) use($tag) {
                $tag->articles()->save($article);
            });
        });
          
        factory(App\Comment::class, 0)->create();
    }
}
