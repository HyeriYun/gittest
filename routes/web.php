<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/article', 'ArticleController');
Route::get('/user/{data}/article', 'ArticleController@indexBy');
Route::get('/article/category/{data}', 'ArticleController@indexBy');
Route::get('/tag/{data}', 'ArticleController@indexBy' );
Route::get('/tag', 'ArticleController@tagIndex' );
Route::get('/search', 'ArticleController@search' );

Route::resource('/article/{p_id}/comment', 'CommentController');
Route::get('/user/{u_id}/comment', 'CommentController@index');

//로그인 되어있어야 한다
Route::group(['middleware' => 'auth'],function(){
        //유저정보
        Route::resource('/user', 'UserController');
        //글쓰기
        Route::get('/article/create', 'ArticleController@create');
        //알림창
        Route::get('/user/{u_id}/notification', 'CommentController@notification');
        //좋아요 저장
        Route::post('/favorite/{id}', 'FavController@store');
        //좋아요 목록
        Route::get('/user/{u_id}/fav', 'FavController@index');     
});

