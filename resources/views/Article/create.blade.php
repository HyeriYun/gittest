@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <form role="form" method="POST" action="{{ url('/article') }}" class="writeForm">
             {{ csrf_field() }}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title"><strong>Title</strong></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Write Title of Article Please.." value="{{ old('title') }}">
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="content"><strong>Content</strong></label>
                    <textarea class="form-control" name="content" placeholder="Write Content of Article Please.." rows="10">{{ old('content') }}</textarea>
                </div>
                <div class="form-group col-md-5">
                    <label for="category">Category</label>
                    <select id="category" name="category" class="select">
                        <option value="All" selected>Category ▼</option>
                        @foreach($categories as $category)
                            @if( $category->name == 'All' )
                            @else
                            <option value="{{ $category->name }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group tag-group col-md-7">
                    <div>
                        <label for="tag">Tag</label>
                        <input type="text" id="inputtags" class="form-control"/>
                    </div>
                    <input type="text" class="form-control hidden" id="pushtags" name="tags"/>
                </div>
                <div id="tags" class="form-group"></div>
                <div class="form-group secret">
                    <label for="secret">Notice</label>
                    <input type="checkbox" id="notice" name="notice">
                    <label for="secret">Secret</label>
                    <input type="checkbox" id="secret" name="secret">
                </div>
                <div class="form-group button">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="reset" class="btn btn-default" onclick="history.go(-1);">Cancel</button>
                </div>
             </form>
             <script>
                $('input#inputtags').on('keypress', function(e){
                    var result = "";
                    if(e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 ){
                        e.preventDefault();
                        $result = "#" + $(this).val();
                        if( $('div#tags').text().indexOf( $result+"," ) == -1 && $result!="#" ){
                            $('div#tags').append('<span class="label label-success tagname">'+$result +'<a href="#" class="removetag"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span><span class="hidden">,</span>');
                        }
                        $(this).val('');
                        console.log($('div#tags').text().replace(/,/g,''));
                        $('input#pushtags').val( $('div#tags').text().replace(/,/g,'') );
                    };
                });
                $('div#tags').on('click', 'a' ,function(){
                    $(this).parent().remove();
                    console.log($('div#tags').text().replace(/,/g,''));
                    $('input#pushtags').val( $('div#tags').text().replace(/,/g,'') );
                });
            </script>
        </div>
    </div>
</div>
@endsection