@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <form role="form" method="POST" action="{{ url('/article') . '/' . $article->id }}" class="writeForm">
             {{ method_field('put') }}
             {{ csrf_field() }}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title"><strong>Title</strong></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Write Title of Article Please.." value="{{ $article->title }}">
                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group"> 
                    <label for="content"><strong>Content</strong></label>
                    <textarea class="form-control" name="content" placeholder="Write Content of Article Please.." rows="10">@if($errors->any()){{ old('content') }}@else{{ $article->content }}@endif</textarea>
                </div>
                <div class="form-group col-md-5">
                    <label for="category">Category</label>
                    <select id="category" name="category" class="select">
                        <option value="All" selected>Category ▼</option>
                        @foreach($categories as $category)
                            @if( $category->name == 'All' )
                            @else
                                @if( $category->name == $selected->name )
                                <option value="{{ $category->name }}" selected>{{ $category->name }}</option>
                                @else
                                <option value="{{ $category->name }}">{{ $category->name }}</option>
                                @endif
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group tag-group col-md-7">
                    <div>
                        <label for="tag">Tag</label>
                        <input type="text" id="inputtags" class="form-control"/>
                    </div>
                    <input type="text" class="form-control hidden" id="pushtags" name="tags" value=""/>
                </div>
            <!---div#tags-->
                <div id="tags" class="col-md-12 form-group">@foreach($tags as $tag)<span class="label label-success tagname">#{{ $tag->name }}<a href="#" class="removetag"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span><span class="hidden">,</span>@endforeach</div>
            <!---end-->
                <div class="form-group secret">
                    <label for="secret">Notice</label>
                    @if( $article->notice_chk == 'on' )
                        <input type="checkbox" id="notice" name="notice" checked>
                    @else
                        <input type="checkbox" id="notice" name="notice">
                    @endif
                    <label for="secret">Secret</label>
                    @if( $article->secret_chk == 'on' )
                        <input type="checkbox" id="secret" name="secret" checked>
                    @else
                        <input type="checkbox" id="secret" name="secret">
                    @endif
                </div>
                <div class="form-group button">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="reset" class="btn btn-default" onclick="history.go(-1);">Cancel</button>
                </div>
             </form>
             <script>
                //태그제어
                $('input#inputtags').on('keypress', function(e){
                    var result = "";
                    if(e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 ){
                        e.preventDefault();
                        $result = "#" + $(this).val();
                        if( $('div#tags').text().indexOf( $result+"," ) == -1 && $result!="#" ){
                            $('div#tags').append('<span class="label label-success tagname">'+$result +'<a href="#" class="removetag"><i class="fa fa-times-circle" aria-hidden="true"></i></a></span><span class="hidden">,</span>');
                        }
                        $(this).val('');
                        console.log($('div#tags').text().replace(/,/g,''));
                        $('input#pushtags').val( $('div#tags').text().replace(/,/g,'') );
                    };
                });
                $('div#tags').on('click', 'a' ,function(){
                    $(this).parent().remove();
                    console.log($('div#tags').text().replace(/,/g,''));
                    $('input#pushtags').val( $('div#tags').text().replace(/,/g,'') );
                });

                $('button[type="submit"]').on('click', function(e){
                    // Secret && Notice 막아야
                       if( $('input#notice').is(":checked") && $('input#secret').is(":checked") ){
                            e.preventDefault();
                            alert('공지글은 반드시 공개상태여야합니다!');
                       }else{
                           
                       }
                });
             </script>
        </div>
    </div>
</div>
@endsection