@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Articles</strong>
                    <ul class="nav nav-tabs category">
                        @foreach($categories as $category)
                            @if( $category->id == 0 )
                                <li><a href="{{ url('/article') }}" id="{{ $category->name }}">{{ $category->name }}</a></li>
                            @else
                                <li><a href="{{ url('/article/category') . '/' . $category->id }}" id="{{ $category->name }}">{{ $category->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                    <form role="form" method="GET" action="{{ url('/search') }}" class="search">
                        <div class="input-group">
                        @if( $field == 'title' )
                            <select class="select main" name="field">
                                <option value="title" selected>제목</option>
                                <option value="content">내용</option>
                            </select>
                        @else
                            <select class="select main" name="field">
                                <option value="title">제목</option>
                                <option value="content" selected>내용</option>
                            </select>
                        @endif
                            <input type="text" class="form-control search" name="search" placeholder="Search for..." value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <table class="table" style="table-layout:fixed">
                    <tr>
                        <th class="col-md-2">Date</th>
                        <th class="col-md-7">Article Title</th>
                        <th class="col-md-3">User</th>
                    </tr>
                    @foreach($notices as $notice)
                    <tr class="notice">
                        <td>
                            <i class="fa fa-exclamation" aria-hidden="true"></i>
                            <span class="new">Notice</span>
                        </td>
                        <td colspan="2">
                            <a href="{{ url('/article') . '/' . $notice->id }}">
                                {{ $notice->title }}
                            </a>
                        </td>  
                    </tr>
                    @endforeach
                    @foreach($articles as $article)
                        @if( $article->secret_chk == 'on' )
                            @if( $article->user->id == Auth::id() )
                                <tr>
                                    <td class="date">{{ $article->created_at }}</td>
                                    <td style="word-break:break-all">
                                        <a href="{{ url('/article') . '/' . $article->id }}">
                                            {{ $article->title }}
                                        </a>
                                    @if( $nowstr - $article->created_at->format('Ymd') < 1 )
                                        <span class="new">new!!</span>
                                    @endif
                                    @if( $article->secret_chk == 'on' )
                                        <i class="fa fa-lock lock" aria-hidden="true"></i>
                                    @endif 
                                    <span class="cnt"> ( {{ $comments->where('article_id', $article->id)->count() }} ) </span>
                                        </td>
                                    <td class="user">
                                        <a href="">
                                            {{ $article->user->name }}
                                        </a>
                                        <ul class="aboutUser">
                                            <li><a href="{{ url('/user') . '/' . $article->user->id }}">User's Info</a></li>
                                            <li><a href="{{ url('/user') . '/' . $article->user->id . '/article' }}">User's Articles</a></li>
                                            <li><a href="{{ url('/user') . '/' . $article->user->id . '/comment' }}">User's Comments</a></li>
                                        </ul>
                                    </td>     
                                </tr>
                            @else
                                
                            @endif
                        @else
                            <tr>
                                <td class="date">{{ $article->created_at }}</td>
                                <td>
                                    <a href="{{ url('/article') . '/' . $article->id }}">
                                        {{ $article->title }}
                                    </a>
                                        @if( $nowstr - $article->created_at->format('Ymd') < 1 )
                                             <span class="new">new!!</span>
                                        @endif
                                    <span class="cnt"> ( {{ $comments->where('article_id', $article->id)->count() }} ) </span>
                                    </td>
                                <td class="user">
                                    <a href="">
                                        {{ $article->user->name }}
                                    </a>
                                    <ul class="aboutUser">
                                        <li><a href="{{ url('/user') . '/' . $article->user->id }}">User's Info</a></li>
                                        <li><a href="{{ url('/user') . '/' . $article->user->id . '/article' }}">User's Articles</a></li>
                                        <li><a href="{{ url('/user') . '/' . $article->user->id . '/comment' }}">User's Comments</a></li>
                                    </ul>
                                </td>     
                            </tr>
                        @endif
                    @endforeach
                </table>
                <div class="panel-body" align="center">
                 {{ $articles->appends(request()->except('page'))->links() }}
                 @if(Auth::check())
                    <p><a class="btn btn-default write" href="{{ url('/article/create') }}" role="button">Write</a> </p>
                 @endif
                </div>       
            </div>
        </div>
    </div>
    <script>
        $('td.user > a').on('click',function(e){
            e.preventDefault();
            $('ul.aboutUser').hide();
            $(this).parent().find('ul.aboutUser').show();
        });
        var a="";
        $('td.user').on('mouseenter', function(){
            a = true;
        });
        $('td.user').on('mouseleave', function(){
            a = false;
        });
        $('*').on('click',function(){
           if( !a ){
               $('ul.aboutUser').hide();
           };
        });
    </script>
</div>
@endsection