@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if( Request::segment(1) == 'tag' )
            <div>
                <h3 align="center" class="tag_notice">
                    <i class="fa fa-hashtag" aria-hidden="true"></i><br/>
                    {{ '‘ '.$tagname.' ’ 태그의 글 목록 '. $tag->articles()->count() .'개' }}
                </h3>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Articles</strong>
                <!---Current Url에 따라 나뉜다.-->
                    @if( Request::segment(3) == 'fav' )
                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                    <span class="new default">{{$user->name}}'s Favorite</span>
                    @endif
                    @if( Request::segment(3) == 'article' )
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    <span class="new default">Written by. {{$user->name}}</span>
                    @endif
                    @if( Request::segment(1) == 'tag' )
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <span class="tag">{{ $tagname }}</span>
                    @endif
                </div>
                <table class="table" style="table-layout:fixed">
                    <tr>
                        <th class="col-md-2">Date</th>
                        <th class="col-md-7">Article Title</th>
                        <th class="col-md-3">User</th>
                    </tr>
                    @foreach($articles as $article)
                        @if( $article->secret_chk == 'on' )
                            @if( $article->user->id == Auth::id() )
                                <tr>
                                    <td class="date">{{ $article->created_at }}</td>
                                    <td style="word-break:break-all">
                                        <a href="{{ url('/article') . '/' . $article->id }}">
                                            {{ $article->title }}
                                        </a>
                                    @if( $nowstr - $article->created_at->format('Ymd') < 1 )
                                        <span class="new">new!!</span>
                                    @endif
                                    @if( $article->secret_chk == 'on' )
                                        <i class="fa fa-lock lock" aria-hidden="true"></i>
                                    @endif 
                                    <span class="cnt"> ( {{ $comments->where('article_id', $article->id)->count() }} ) </span>
                                        </td>
                                    <td class="user">
                                        <a href="">
                                            {{ $article->user->name }}
                                        </a>
                                        <ul class="aboutUser">
                                            <li><a href="{{ url('/user') . '/' . $article->user->id }}">User's Info</a></li>
                                            <li><a href="{{ url('/user') . '/' . $article->user->id . '/article' }}">User's Articles</a></li>
                                            <li><a href="{{ url('/user') . '/' . $article->user->id . '/comment' }}">User's Comments</a></li>
                                        </ul>
                                    </td>     
                                </tr>
                            @else
                                
                            @endif
                        @else
                            <tr>
                                <td class="date">{{ $article->created_at }}</td>
                                <td>
                                    <a href="{{ url('/article') . '/' . $article->id }}">
                                        {{ $article->title }}
                                    </a>
                                        @if( $nowstr - $article->created_at->format('Ymd') < 1 )
                                             <span class="new">new!!</span>
                                        @endif
                                    <span class="cnt"> ( {{ $comments->where('article_id', $article->id)->count() }} ) </span>
                                    </td>
                                <td class="user">
                                    <a href="">
                                        {{ $article->user->name }}
                                    </a>
                                    <ul class="aboutUser">
                                        <li><a href="{{ url('/user') . '/' . $article->user->id }}">User's Info</a></li>
                                        <li><a href="{{ url('/user') . '/' . $article->user->id . '/article' }}">User's Articles</a></li>
                                        <li><a href="{{ url('/user') . '/' . $article->user->id . '/comment' }}">User's Comments</a></li>
                                    </ul>
                                </td>     
                            </tr>
                        @endif
                    @endforeach
                </table>
                <div class="panel-body" align="center">
                 {{ $articles->links() }}
                 @if(Auth::check())
                    <p><a class="btn btn-default write" href="{{ url('/article/create') }}" role="button">Write</a> </p>
                 @endif
                </div>       
            </div>
        </div>
    </div>
    <script>
        $('td.user > a').on('click',function(e){
            e.preventDefault();
            $('ul.aboutUser').hide();
            $(this).parent().find('ul.aboutUser').show();
        });
        var a="";
        $('td.user').on('mouseenter', function(){
            a = true;
        });
        $('td.user').on('mouseleave', function(){
            a = false;
        });
        $('*').on('click',function(){
           if( !a ){
               $('ul.aboutUser').hide();
           };
        });
    </script>
</div>
@endsection