@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading title">
                        <strong>{{ $article->title }}</strong>
                        @if( $article->notice_chk == 'on' )
                            <i class="fa fa-exclamation" aria-hidden="true"></i> <span>Notice</span>
                        @endif
                        @if( $article->secret_chk == 'on' )
                            <i class="fa fa-lock lock" aria-hidden="true"></i> <span>secret!</span>
                        @endif 
                    </div>
                    <div class="panel-body">
                        <p class="category">tag :: 
                            @foreach( $tags as $tag )
                                <a href="{{ url('tag') . '/' . $tag->name }}" class="taglist"> #{{ $tag->name }} </a>
                            @endforeach
                        </p>
                        <p class="article_content"> {{ $article->content }} </p>
                        <p align="right" class="writer"><br/><br/>─ written by. {{ $article->user->name }}</p>
                        @if( $category->name != 'All' )
                        <p align="right" class="category">Category in. {{ $category->name }}</p>
                        @endif
                        <!--- 좋아요! -->
                            @if( Auth::check() )
                                <div class="favorite">
                                @if( $fav == 1 )
                                    <a href="#" id="fav" class="favorite" onclick="return false;">
                                        <span>좋아요!</span><br/>
                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                        <span class="cnt_fav">{{ $article->users()->count() }}</span>
                                    </a>
                                    <input type="checkbox" name="fav" class="fav hidden" value="fav" checked/>
                                @else
                                    <a href="#" id="no-fav" class="favorite" onclick="return false;">
                                        <span>좋아요!</span><br/>
                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                        <span class="cnt_fav">{{ $article->users()->count() }}</span> 
                                    </a>
                                    <input type="checkbox" name="fav" class="fav hidden" value="fav"/>
                                @endif
                                </div>
                            @endif 
                            <script>
                            $('a.favorite').on('click',function(){
                                $('input.fav').click();
                                var dataArr = { fav : $('input.fav:checked').val() }
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    type:'POST',
                                    url:'{{ url('') . '/favorite/' . $article->id }}',
                                    data:dataArr,
                                    success:function(data){
                                         $('a.favorite i').toggleClass('fa-heart-o').toggleClass('fa-heart');
                                        $('a.favorite span.cnt_fav').replaceWith('<span class="cnt_fav">'+data+'</span>')
                                    },
                                    error:function(){
                                        console.log('error');
                                    }
                                });
                            });
                            </script>
                         <!---Article delete/modify-->
                        @if($article->user_id==Auth::id())
                         <form role="form" method="POST" action="{{ url('/article') . '/' . $article->id }}">
                                     {{ method_field('delete') }}
                                     {{ csrf_field() }}  
                            <a class="btn btn-default" href="{{ url('/article') . '/' . $article->id . '/edit' }}">
                                <i class="fa fa-eraser" aria-hidden="true"></i> mod
                            </a>
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-trash" aria-hidden="true"></i> del
                            </button>
                         </form>
                        @endif                       
                    </div>

                        <!---Comment-->
                    <table class="table" style="table-layout:fixed">
                         @foreach($article->comments as $comment)
                            <tr class="comment">
                                <td class="col-md-2"><strong>{{ $comment->user->name }}</strong></td>
                                <td class="col-md-9" style="word-break:break-all">{{ $comment->content }}</td>
                                <td class="col-md-1">
                                @if($comment->user_id==Auth::id())
                                 <form role="form" method="POST" action="{{ url('/article') . '/' . $article->id . '/comment' . '/' . $comment->id}}">
                                     {{ method_field('delete') }}
                                     {{ csrf_field() }}
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-trash" aria-hidden="true"></i> del
                                    </button>
                                 </form>
                                @endif
                                </td>
                            </tr>                           
                         @endforeach
                    </table>
                </div>
                <!---Comment Input-->
                <div class="panel panel-default">
                    <div class="panel-body">
                    @if(Auth::check())
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/article') . '/' . $article->id . '/comment' }}">
                        {{ csrf_field() }}
                        <table class="table nobd" style="table-layout:fixed">
                            <tr>
                                <td class="col-md-2"> 
                                    {{ Auth::user()->name }}
                                </td>
                                <td class="col-md-8" style="word-break:break-all">
                                    <textarea class="form-control" name="content" rows="3"></textarea>
                                </td>
                                <td class="col-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        Add<br/>Comment
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </form>
                    @endif
                    <a href="{{ url('/article') }}" class="golink"><strong>Go back to List</strong></a>
                    </div>
                </div>
            <div class="prev_next">
                <a href="{{ url('/article') . '/' . $prev }}" class="prev {{ $prev==null ? 'hidden' : '' }}">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                <strong>Prev</strong>
                </a>
                <a href="{{ url('/article') . '/' . $next }}" class="next {{ $next==null ? 'hidden' : '' }}">
                <strong>Next</strong>
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection