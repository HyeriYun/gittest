@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h1 class="adminPage"><span>{{ $user->name }}</span>'s Page</h1></div>
            <div class="panel-body">
                <table class="table adminPage">
                    <tr>
                        <td colspan="3">
                            <h2>Name</h2>
                            <p>{{ $user->name }}</p>
                            <h2>Email</h2>
                            <p>{{ $user->email }}</p>
                            <h2>Join date</h2>
                            <p>{{ $user->created_at }}</p>
                        </td>
                    </tr>
                @if( $user->id == Auth::id() )
                    <tr>
                        <td>
                            <a href="{{ url('/user') . '/' . Auth::user()->id . '/edit' }}">
                                <h2>회원정보 수정</h2>
                                <p>modify info</p>
                            </a>
                        </td>
                @endif
                        <td>
                            <a href="{{ url('/user') . '/' . $user->id . '/article' }}">
                                <h2>작성 글 보기</h2>
                                <p>Article by.{{ $user->name }}</p>
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('/user') . '/' . $user->id . '/comment' }}">
                                <h2>작성 댓글 보기</h2>
                                <p>comment by.{{ $user->name }}</p>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection