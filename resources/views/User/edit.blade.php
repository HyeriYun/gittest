@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <form role="form" class="user_info" method="POST" action="{{ url('/user') . '/' . $user->id }}">
             {{ method_field('put') }}
             {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} user_info">
                    <label for="name"><strong>Name</strong></label>
                    @if( !$errors->any() )
                        <input type="text" class="form-control user_info" id="name" name="name" placeholder="Name" value="{{ $user->name }}">
                    @else 
                        <input type="text" class="form-control user_info" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                    @endif
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} user_info">
                    <label for="email"><strong>Email</strong></label>
                    @if ( !$errors->any() )
                        <input type="email" class="form-control user_info" id="email" name="email" placeholder="Email" value="{{ $user->email }}">
                    @else 
                        <input type="email" class="form-control user_info" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                    @endif
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} user_info">
                    <label for="password"><strong>Original Password</strong></label>
                    <input type="password" class="form-control user_info" id="password" name="password" placeholder="Original Password" value="{{ old('password') }}">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }} user_info">
                    <label for="new_password"><strong>New Password</strong></label>
                    <input type="password" class="form-control user_info" id="new_password" name="new_password" placeholder="New Password" value="{{ old('new_password') }}">
                    @if ($errors->has('new_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('new_password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} user_info">
                    <label for="password_confirmation"><strong>Password Confirmation</strong></label>
                    <input type="password" class="form-control user_info" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" value="">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group user_info" align="center">
                    <button type="submit" class="btn btn-default">Modify</button>
                    <button type="reset" class="btn btn-default" onclick="history.go(-1);">Cancel</button>
                </div>
             </form>
             
        </div>
    </div>
</div>
@endsection