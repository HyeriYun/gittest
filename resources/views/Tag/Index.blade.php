@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="page-header">
                    <h3 align="center" class="tag_notice">
                    <i class="fa fa-hashtag" aria-hidden="true"></i><br/>
                        Tag Cloud<br/>
                        <span>Total {{ $tags->count() }}</span>
                    </h3>
                </div>
                <div class="panel-body" align="center" style="margin-bottom:50px;">
                    @foreach( $tags as $tag )
                        @if( $tag->articles()->count() >= 100 )
                            <a href="{{ url('tag') . '/' . $tag->name }}" class="tagCloud toomany">
                        @elseif( $tag->articles()->count() >= 50 )
                            <a href="{{ url('tag') . '/' . $tag->name }}" class="tagCloud many">
                        @else
                            <a href="{{ url('tag') . '/' . $tag->name }}" class="tagCloud">
                        @endif
                            {{ $tag->name }}
                            <span>({{ $tag->articles()->count() }})</span>
                            </a>   
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection