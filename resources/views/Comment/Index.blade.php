@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                         Comments
                    </strong>
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        <span class="new default">Written by. {{$user->name}}</span>
                </div>
                <table class="table" style="table-layout:fixed">
                    <tr>
                        <th class="col-md-2">Date</th>
                        <th class="col-md-4">Comment Content</th>
                        <th class="col-md-3">Article</th>
                        <th class="col-md-3">User</th>
                    </tr>
                @foreach($comments as $comment)
                    <tr class="commentlist">
                        <td class="col-md-2 date">{{ $comment->created_at }}</td>
                        <td class="col-md-3" style="word-break:break-all">
                            {{ $comment->content }}
                            @if( $nowstr - $comment->created_at->format('Ymd') < 1 )
                            <span class="new">new!!</span>
                            @endif
                        </td>
                        <td class="col-md-5" style="word-break:break-all">
                            title : 
                            <a href="{{ url('/article') . '/' . $comment->article_id }}">
                                {{ $comment->article->title }}
                            </a>
                            <p><i class="fa fa-pencil" aria-hidden="true"></i>
                             {{ $comment->article->user->name }}</p>
                        </td>
                        <td class="col-md-2">{{ $comment->user->name }}</td>
                    </tr>
                @endforeach
                </table>
                <div class="panel-body" align="center">
                 {{ $comments->links() }}
                </div>       
            </div>
        </div>
    </div>
</div>
@endsection