@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                         {{ $user->name }}'s Notification
                    </strong>
                </div>
                <table class="table">
                    <tr>
                        <th class="col-md-2">Date</th>
                        <th class="col-md-7">Comment Content</th>
                        <th class="col-md-3">User Name</th>
                    </tr>
                @foreach( $comments as $comment )
                  
                    <tr>
                        <td class="date">{{ $comment->created_at }}</td>
                        <td class="cm_content">
                            <a href="{{ url('/article') . '/' . $comment->article_id }}">
                            {{ $comment->content }}
                            </a>
                            @if( $nowstr - $comment->article->created_at->format('Ymd') < 1 )
                                <span class="new">new!!</span>
                            @endif
                        </td>
                        <td>{{ $comment->user->name }}</td>
                    </tr>
                    
                @endforeach
                </table>
                <div class="panel-body" align="center">
                </div>       
            </div>
        </div>
    </div>
</div>
@endsection